<!DOCTYPE html>
<html lang="en">
	<head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Activity s05</title>
	</head>
	<body>
		<?php session_start(); ?>

		<?php if(isset($_SESSION['users'])) : ?>

			<p>Hello <?= $_SESSION['users'][0]->email ?></p>

			<form action="./server.php" method="post" >
				<input type="hidden" name="action" value="clear" />
				<button type="submit">Logout</button>
			</form>
               
		<?php else: ?>

			<form action="./server.php" method="post" >
				<input type="hidden" name="action" value="login" />
				Email:<input type="email" name="email" />
				Password:<input type="password" name="password" />
				<button type="submit">Login</button>
			</form>

		<?php endif; ?>
	</body>
</html>